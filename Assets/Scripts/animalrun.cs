﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animalrun : MonoBehaviour
{
    public float MovementSpeed;
    public float timer;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, timer);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.back * Time.deltaTime * MovementSpeed);
    }

    void OnTriggerEnter()
    {
        if (GameObject.FindWithTag("food"))
        {
            Destroy(this.gameObject);
        }
    }
}
