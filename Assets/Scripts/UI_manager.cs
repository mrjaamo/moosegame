﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_manager : MonoBehaviour
{
    public GameObject panel;
    public GameObject spawner;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        panel.SetActive(false);
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
  
    }


    void OnTriggerEnter()
    {
        if (GameObject.FindWithTag("animal"))
        {
            panel.SetActive(true);
            Destroy(spawner);
            Destroy(player);
            Invoke("Restart_game",5);
        }
    }

    void Restart_game()
    {
        SceneManager.LoadScene(0);
    }
}
