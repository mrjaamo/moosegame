﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animalspawn : MonoBehaviour
{
    //gameobject for spawning 
    public GameObject[] animal;
    public int id;
    //set custom range for random position
    public float MinX = -20;
    public float MaxX = 20;
    public float spawntime;

    private void Start()
    {
        InvokeRepeating("SpawnObject", spawntime,spawntime);
    }

    public void Update()
    {

    }

    void SpawnObject()
    {
        id = Random.Range(0,animal.Length);
        float x = Random.Range(MinX, MaxX);
        Instantiate(animal[id], new Vector3(x, 0, 20), animal[id].transform.rotation);
    }
}
