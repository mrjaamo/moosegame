﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playermovement : MonoBehaviour
{
    public float movementspeed;
    public float inputx;
    public float max_x;
    public float min_max;
    public GameObject food;


    // Start is called before the first frame update
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        inputx = Input.GetAxis("Horizontal");


        transform.Translate(Vector3.right * inputx * movementspeed * Time.deltaTime);

        if (transform.position.x > max_x)
        {
            transform.position = new Vector3(max_x, transform.position.y, transform.position.z);
        }

        if (transform.position.x < min_max)
        {
            transform.position = new Vector3(min_max, transform.position.y, transform.position.z);
        }


        if (Input.GetButtonDown("Fire1") == true)
        {
            shoot();
        }
    }
    public void shoot()
    {
        Instantiate(food,transform.position + new Vector3(0, 0, 1), food.transform.rotation);
    }


}
